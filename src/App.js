import React, {useEffect} from 'react';
import './Sass/App.scss';
import './Css/App.css';
import Card from './Components/Card/Card';

import { fetchImages } from "./redux/image/imageSplice";

import { useDispatch } from "react-redux";


const App = () => {

  const dispatch = useDispatch();

  useEffect(() => {
      dispatch(fetchImages())
  }, [dispatch])




    return (
      <main className="main">
              <Card />
      </main>
    );
  
}

export default App;
