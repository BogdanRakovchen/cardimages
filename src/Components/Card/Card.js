import React, { useState } from "react";
import '../../Css/Card.css';

import { data, showMobileModalState, showDesctopModalState, 
showModalDesctop, showModalMobile } from "../../redux/image/imageSplice";

import { useDispatch, useSelector } from "react-redux";

import CardModal from "./CardModal";



const Card = () => {  

    const images = useSelector(data);
    const showStateMobileModal = useSelector(showMobileModalState)
    const showStateDesctopModal = useSelector(showDesctopModalState)

    const dispatch = useDispatch()

    let arrayWithImagesOneTopDepth = images.flat(1);

    const [imageState, setImageState] = useState('');


   const clickShowModal = (e) => {
  
    window.scrollTo(0,0);

    if(window.screen.width < 768) {
        setImageState(e.target.src)
        dispatch(showModalMobile())
    } else {
        setImageState(e.target.src)
        dispatch(showModalDesctop())
    }
    
  
   

   }

   return (
        <React.Fragment>

                {showStateMobileModal && <CardModal imageState={imageState} />}

             
           <div className={showStateMobileModal ? 'card-wrapper_hidden' : 'card-wrapper_show' }>


             
             
             <header className="header">
              <h1>Gallery</h1>
            </header>
            <div>

            {showStateDesctopModal && <CardModal imageState={imageState} />}
              
            
                 
                    
                    {arrayWithImagesOneTopDepth.map(el => {
    
                      
                            return <img key={el.id} src={el.url} 
                            className="card-wrapper__image" 
                            onClick={clickShowModal} />  
                    
                      
                    })}
    
            </div>
            <footer className="footer">
                <div className="footer__line"></div>
                <div className="footer__span-style">@ 2021-2022</div>
            </footer>
            
            </div>

            </React.Fragment>
        )
                
}


export default Card;