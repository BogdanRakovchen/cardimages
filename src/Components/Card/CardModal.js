import React, {useState} from 'react';

import '../../Css/CardModal.css';

import { data, hiddenModalMobile, hiddenModalDesctop } from "../../redux/image/imageSplice";

import { comments, addComment, deletedComment } from '../../redux/comments/commentsSplice';

import { useSelector, useDispatch } from "react-redux";

import { nanoid } from '@reduxjs/toolkit';





const CardModal = ({ imageState }) => {

    //const images = useSelector(data);

    const commentsData = useSelector(comments);

    const dispatch = useDispatch()

    const [name, setName] = useState('');
    const [comment, setComment] = useState('');


    const closeCardModal = () => {
        if(window.screen.width < 768) {
            dispatch(hiddenModalMobile())
        } else {
            dispatch(hiddenModalDesctop())
        }
    }

   
   

    const handleChangeName = (event) => {
        setName(event.target.value);
        
    }

    const handleChangeComment = (event) => {
        setComment(event.target.value);
        
    }

     

    const handleSubmit = (event) => {
        event.preventDefault()
        
        dispatch(addComment({name, comment, id: nanoid()}))
        setName('');
        setComment('');
       
    }

   
    return (



        <div className="card-modal card-modal_block_show">
             
             <div className="dialog-wrapper">

                    <div className="dialog-wrapper__modal">

                   
                    
                    <div className="modal-image">
                    

                        <div className="modal-image__lines-wrapper" onClick={closeCardModal}>
                            <div className="modal-image__line-1"></div>
                            <div className="modal-image__line-2"></div>
                        </div>
                        <img className="modal-image__img" src={imageState}/>
                        </div>


                    
               
         
                <div className="modal-comments">
                  
                    {commentsData.map((el) => {
                       
                            return (
                                <ul className="modal-comments__list" key={el.id} onClick={() => dispatch(deletedComment(el.id))} >
                                    
                                 
                                    <li className="modal-comments__item">{el.name}</li>
                                    <li className="modal-comments__item">{el.comment}</li>
                               </ul>
                               )
                        
                     
                    })}
                  
                 
                </div>
        </div>
        <form className="form" onSubmit={handleSubmit} action="https://boiling-refuge-66454.herokuapp.com/images" method="POST">
                <div className="form-wrapper">
                    {/*form__form-wrapper  */}
                     <input type="text" name="name" value={name} onChange={handleChangeName} 
                    placeholder="Ваше имя" className="form-wrapper__input"/>

                    <input type="text" name="comment" value={comment} onChange={handleChangeComment} 
                    placeholder="Ваш комментарий" className="form-wrapper__input"/> 

              


                    <button type="submit" className="form-wrapper__button">Оставить комментарий</button>
                </div>
            </form>
        </div>
        </div>

    )
}

export default CardModal;