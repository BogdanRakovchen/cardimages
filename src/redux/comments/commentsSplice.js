import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
    comments: []
}


const commentsSlice = createSlice({
    name: 'comments',
    initialState,
    reducers: {
        addComment(state, action) {
           

            state.comments.push(action.payload);

            
        },

        deletedComment(state, action) {
            let newState = state.comments.filter(el => el.id !== action.payload)
            state.comments = newState;
        }
    },

   
})

export const {addComment, deletedComment} = commentsSlice.actions;

export default commentsSlice.reducer;
export const comments = state => state.comments.comments.map(el => el)