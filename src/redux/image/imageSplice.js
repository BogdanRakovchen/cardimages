import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
    images: [],
    showMobileModalState: false,
    showDesctopModalState: false,

   

}

export const fetchImages = createAsyncThunk('images/fetchImages', async () => {
    const response = await fetch(`https://boiling-refuge-66454.herokuapp.com/images`);

    return response.json()
    
})




const imageSlice = createSlice({
    name: 'images',
    initialState,
    reducers: {
      showModalMobile(state, action) {
        state.showMobileModalState = !state.showMobileModalState
     
      
      },

      showModalDesctop(state, action) {
        state.showDesctopModalState = !state.showDesctopModalState
      },
   
      hiddenModalMobile(state, action) {
        state.showMobileModalState = !state.showMobileModalState
      },

      hiddenModalDesctop(state, action) {
        state.showDesctopModalState = !state.showDesctopModalState
      },

      
    },

    extraReducers(builder) {
        builder
          .addCase(fetchImages.pending, (state, action) => {
            state.status = 'loading'
          })
          .addCase(fetchImages.fulfilled, (state, action) => {
            state.status = 'succeeded'
            // Add any fetched posts to the array
           state.images.push(action.payload)
      
          })
          .addCase(fetchImages.rejected, (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
          })
         
      }
      
})

export default imageSlice.reducer
export const {showModalMobile, showModalDesctop, hiddenModalMobile, hiddenModalDesctop} = imageSlice.actions
export const data = state => state.images.images.map(el => el)

export const showMobileModalState = state => state.images.showMobileModalState
export const showDesctopModalState = state => state.images.showDesctopModalState