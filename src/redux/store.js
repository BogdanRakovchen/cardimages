import { configureStore } from "@reduxjs/toolkit";

import imageSplice from "./image/imageSplice";
import commentsSplice from './comments/commentsSplice';

export default configureStore({
    reducer: {
        images: imageSplice,
        comments: commentsSplice
    }
})